import { MomentService } from "./services/moment.service";

declare let moment: any;

export class YearMonthGroupable {
  public monthYear: string;
}

export class Customer {
  constructor(public _id: string,
              public fullName: string,
              public pricingPlan: CustomerPlan) {}
}

export class CustomerPlan {
  constructor(public name: string,
              public monthlyFee: number,
              public dayHalfHourFee: number,
              public nightHalfHourFee: number,
              public limit: number) {}
}

export class Parking extends YearMonthGroupable {
  constructor(public _id: string,
              public customer: string,
              public year: number,
              public month: number,
              public parkingHouse: string,
              public arrival: string,
              public departure: string) {
    this.arrival = moment.unix(arrival).utcOffset(0).format("DD.MM HH:mm");
    this.departure = moment.unix(departure).utcOffset(0).format("DD.MM HH:mm");
    this.monthYear = MomentService.toMonthYear(this.month, this.year);
  }
}

export class ParkingInvoice extends YearMonthGroupable {
  constructor(public _id: string,
              public customer: string,
              public year: number,
              public month: number,
              public dayHalfHours: number,
              public nightHalfHours: number,
              public totalFee: number) {
    this.monthYear = MomentService.toMonthYear(this.month, this.year);
  }
}
