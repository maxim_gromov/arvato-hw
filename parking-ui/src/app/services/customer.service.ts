import { Observable } from 'rxjs/Observable';

import { DefaultService } from './default.service';
import { Parking, Customer, ParkingInvoice, CustomerPlan } from "../app.domain";

export class CustomerService extends DefaultService {

  private customerApi = 'http://localhost:3000/api/customer';

  getAll(): Observable<Customer[]> {
    return this.http.get(this.customerApi)
      .map(this.getArray)
      .map(this.toCustomers)
      .catch(this.handleError);
  }

  getCustomerParkings(customerId: string): Observable<Parking[]> {
    return this.http.get(`${this.customerApi}/${customerId}/parking`)
      .map(this.getArray)
      .map(this.toParkings)
      .catch(this.handleError);
  }

  getCustomerInvoices(customerId: string): Observable<ParkingInvoice[]> {
    return this.http.get(`${this.customerApi}/${customerId}/invoice`)
      .map(this.getArray)
      .map(this.toInvoices)
      .catch(this.handleError);
  }

  generateInvoice(customerId: string, month: number, year: number): Observable<ParkingInvoice> {
    return this.http.post(`${this.customerApi}/${customerId}/invoice`, {month: month, year: year})
      .map(this.getData)
      .map(CustomerService.toParkingInvoice)
      .catch(this.handleError);
  }

  toParkings(fromAny: any[]): Parking[] {
    let parkings: Parking[] = [];
    fromAny.forEach((parking) => {
      parkings.push(new Parking(
        parking._id,
        parking.customer,
        parking.year,
        parking.month,
        parking.parkingHouse.name,
        parking.arrival,
        parking.departure
      ));
    });
    return parkings;
  }

  toInvoices(fromAny: any[]): ParkingInvoice[] {
    let invoices: ParkingInvoice[] = [];
    fromAny.forEach((invoice) => invoices.push(CustomerService.toParkingInvoice(invoice)));
    return invoices;
  }

  toCustomers(fromAny: any[]): Customer[] {
    let customers: Customer[] = [];
    fromAny.forEach((customer) => {
      customers.push(new Customer(
        customer._id,
        customer.fullName,
        CustomerService.toCustomerPlan(customer.pricingPlan)
      ));
    });
    return customers;
  }

  static toParkingInvoice(invoice: any): ParkingInvoice {
    return new ParkingInvoice(
      invoice._id,
      invoice.customer,
      invoice.year,
      invoice.month,
      invoice.dayHalfHours,
      invoice.nightHalfHours,
      invoice.totalFee
    );
  }

  static toCustomerPlan(fromAny: any): CustomerPlan {
    return new CustomerPlan(
      fromAny.name,
      fromAny.monthlyFee,
      fromAny.dayHalfHourFee,
      fromAny.nightHalfHourFee,
      fromAny.limit
    );
  }

}
