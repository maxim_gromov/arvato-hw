declare let moment: any;

export class MomentService {

  public static toMonthYear(month: number, year: number): string {
    return moment()
      .month(month - 1)
      .year(year)
      .format("MMMM YYYY");
  }

  public static toMoment(fromString: string) :any {
    return moment(fromString, "MMMM YYYY");
  }

  public static sort(a: string, b: string) :number {
    let aMoment = MomentService.toMoment(a),
      bMoment = MomentService.toMoment(b);
    if (aMoment.isBefore(bMoment))
      return -1;
    else
      return 1;
  }

}
