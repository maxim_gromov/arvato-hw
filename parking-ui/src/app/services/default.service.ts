import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

@Injectable()
export class DefaultService {
  constructor (protected http: Http) {}

  protected getData(res: Response) {
    let body = res.json();
    return body || {};
  }

  protected getArray(res: Response) {
    let body = res.json();
    return body || [];
  }

  protected handleError (error: Response | any) {
    if (error instanceof Response) {
      console.log(error);
      const body = error.json() || '';
      const err = body.message || JSON.stringify(body);
      return Observable.throw(err);
    } else {
      return Observable.throw("Error happened");
    }
  }

}
