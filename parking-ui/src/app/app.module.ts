import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {DataListModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {AccordionModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';

import { AppComponent } from './app.component';

import {CustomerService} from "./services/customer.service";
import { ParkingListComponent } from './parking-list/parking-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ParkingListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    DataListModule,
    ButtonModule,
    AccordionModule,
    DialogModule
  ],
  providers: [CustomerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
