import { Component } from '@angular/core';
import { Customer } from "./app.domain";
import { CustomerService } from "./services/customer.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  customers:        Customer[];
  selectedCustomer: Customer;
  error:            any;

  constructor(private customerService: CustomerService) {
    this.customerService.getAll()
      .subscribe(
        customers => this.customers = customers,
        error     => this.error     = error
      );
  }

  loginAs(customer: Customer) {
    this.selectedCustomer = customer;
  }

  logout() {
    this.selectedCustomer = null;
  }

}
