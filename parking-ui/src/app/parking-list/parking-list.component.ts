import { Component, OnInit, Input } from '@angular/core';
import { Customer, CustomerPlan, Parking, ParkingInvoice, YearMonthGroupable } from "../app.domain";
import { CustomerService } from "../services/customer.service";
import { MomentService } from "../services/moment.service";

@Component({
  selector: 'parking-list',
  templateUrl: './parking-list.component.html',
  styleUrls: ['./parking-list.component.css']
})
export class ParkingListComponent implements OnInit {

  @Input() customer: Customer;

  parkings: Parking[] = [];
  invoices: ParkingInvoice[] = [];

  displayDetailedInvoice: boolean = false;
  displayError: boolean = false;
  selectedInvoice: ParkingInvoice;

  dataGroups: any = {};
  monthYears: string[];

  error: any;

  constructor(private customerService: CustomerService) {}

  ngOnInit() {
    this.customerService.getCustomerParkings(this.customer._id)
      .subscribe(parkings => this.processParkingOrInvoices(parkings), error => this.handleError(error));
    this.customerService.getCustomerInvoices(this.customer._id)
      .subscribe(invoices => this.processParkingOrInvoices(invoices), error => this.handleError(error));
  }

  processParkingOrInvoices<T extends YearMonthGroupable>(parkingOrInvoices: T[]) {
    if (parkingOrInvoices instanceof Parking) {
      this.parkings = this.parkings || [];
      this.parkings.concat(parkingOrInvoices);
    } else if (parkingOrInvoices instanceof  ParkingInvoice) {
      this.invoices = this.invoices || [];
      this.invoices.concat(parkingOrInvoices);
    }
    parkingOrInvoices.forEach((parkingOrInvoice) => this.groupParkingOrInvoice(parkingOrInvoice));
    this.generateMonthYearsAndSort();
  }

  groupParkingOrInvoice<T extends YearMonthGroupable>(parkingOrInvoice: T) {
    let group = this.getMonthYearGroup(parkingOrInvoice.monthYear);
    if (parkingOrInvoice instanceof Parking) {
      group.parkings.push(parkingOrInvoice);
    } else {
      group.invoice = parkingOrInvoice;
    }
  }

  getMonthYearGroup(monthYear: string) {
    let monthYearGroup = this.dataGroups[monthYear];
    if (monthYearGroup == null) {
      monthYearGroup = this.dataGroups[monthYear] = {
        parkings: [],
        invoice: null
      };
    }
    return monthYearGroup;
  }

  generateMonthYearsAndSort() {
    if (this.dataGroups) {
      this.monthYears = Object.keys(this.dataGroups);
      this.monthYears.sort(MomentService.sort);
    }
  }

  generateOrShowInvoice(monthYear) {
    let parkingGroup = this.dataGroups[monthYear],
        groupMoment = MomentService.toMoment(monthYear);
    if (!parkingGroup.invoice) {
      this.customerService.generateInvoice(this.customer._id, groupMoment.month() + 1, groupMoment.year())
        .subscribe((invoice) => parkingGroup.invoice = invoice, error => this.handleError(error));
    } else {
      this.displayDetailedInvoice = true;
      this.selectedInvoice = parkingGroup.invoice;
    }
  }

  handleError(error) {
    this.error = error;
    this.displayError = true;
  }

}
