import { ParkingUiPage } from './app.po';

describe('parking-ui App', () => {
  let page: ParkingUiPage;

  beforeEach(() => {
    page = new ParkingUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
