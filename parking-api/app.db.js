// Database connection initialization
let mongoose        = require('mongoose'),
    constants       = require('./app.constants'),

    ParkingHouse    = require('./models/ParkingHouse'),
    Customer        = require('./models/Customer'),
    CustomerPlan    = require('./models/CustomerPlan'),

    service = {
        connect: function () {
            mongoose.connect(constants.MONGO_CONNECT);
            return mongoose.connection;
        },
        populateTestData: function () {
            // DATABASE POPULATION WITH INIT DATA
            let regularCustomer         = { fullName: "John Jehn" },
                premiumCustomer         = { fullName: "Jehn John" },
                ulemisteParkingHouse    = { name: "Ülemiste parking house" },
                tutParkingHouse         = { name: "TUT parking house" };

            let regularPlan = {
                    name:                   "REGULAR",
                    monthlyFee:             0,
                    dayHalfHourFee:         1.5,
                    nightHalfHourFee:       1,
                    limit:                  null
                },
                premiumPlan = {
                    name:                   "PREMIUM",
                    monthlyFee:             20,
                    dayHalfHourFee:         1,
                    nightHalfHourFee:       0.75,
                    limit:                  300
                };

            CustomerPlan.insertMany([regularPlan, premiumPlan], (err, plans) => {
                if (err)
                    throw new Error("Could not create customer plans." + err);

                console.log("Data initialization: created regular and premium customer plans.");

                regularCustomer.pricingPlan = plans[0]._id;
                premiumCustomer.pricingPlan = plans[1]._id;

                Customer.insertMany([regularCustomer, premiumCustomer], (err, customers) => {
                    if (err)
                        throw new Error("Could not create customers.");

                    console.log("Data initialization: created regular and premium customers.");
                });
            });


            ParkingHouse.insertMany([ulemisteParkingHouse, tutParkingHouse], (err, parkingHouses) => {
                if (err)
                    throw new Error("Could not create parking houses.");

                console.log("Data initialization: created 2 parking houses.");
            });
        }
    };

module.exports = service;