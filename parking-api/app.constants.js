let constants = exports;

function defineConstant(name, value) {
    Object.defineProperty(constants, name, {
        value:          value,
        enumerable:     true,
        writable:       false,
    });
}

// APP CONSTANTS
defineConstant("MONGO_CONNECT",     "mongodb://arvato-hw-api:arvatoapipassword@ds117830.mlab.com:17830/parking-api-store");
defineConstant("models", {
    CUSTOMER:                       "Customer",
    CUSTOMER_PLAN:                  "CustomerPlan",
    PARKING:                        "Parking",
    PARKING_INVOICE:                "ParkingInvoice",
    PARKING_HOUSE:                  "ParkingHouse"
});

defineConstant("TIME_FORMAT", "DD.MM.YYYY HH:mm:ss");