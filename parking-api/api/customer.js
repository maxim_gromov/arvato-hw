let express         = require('express'),
    router          = express.Router(),
    customerService = require('../service/customer.service');

/* GET all customers  */
router.get('/', (req, res, next) => {
    customerService.findAllCustomers()
        .then((customers) => res.status(200).json(customers))
        .catch((err) => next(err));
});

/* GET customer parkings  */
router.get('/:id/parking', (req, res, next) => {
    customerService.findCustomerParkings(req.params.id, {}, true)
        .then((customerParkings) => res.status(200).json(customerParkings))
        .catch((err) => next(err));
});

/* GET customer invoices  */
router.get('/:id/invoice', (req, res, next) => {
    customerService.findCustomerInvoices(req.params.id, {})
        .then((customerInvoices) => res.status(200).json(customerInvoices))
        .catch((err) => next(err));
});

/* POST generate customer invoice based on usage  */
router.post('/:id/invoice', (req, res, next) => {
    customerService.generateCustomerInvoice(req.params.id, req.body)
        .then((customerInvoice) => res.status(200).json(customerInvoice))
        .catch((err) => next(err));
});

module.exports = router;
