let express = require('express'),
    router = express.Router(),
    parkingService = require('../service/parking.service');

/* POST to upload parking  */
router.post('/', (req, res, next) => {
    // Here could be some middleware before this, to make sure
    // there are rights to do this
    parkingService.uploadParkingData(req.body)
        .then((newParking) => res.status(200).json(newParking))
        .catch((err) => next(err));
});

module.exports = router;
