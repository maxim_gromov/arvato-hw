let mongoose        = require('mongoose'),
    Schema          = mongoose.Schema,
    constants       = require('../app.constants'),

    ParkingInvoiceSchema = new Schema({
        customer: {
            type: Schema.ObjectId,
            ref: constants.models.CUSTOMER,
            required: true
        },
        year: {
            type: Number,
            required: true
        },
        month: {
            type: Number,
            required: true
        },
        parkings: [
            {
                type: Schema.ObjectId,
                ref: constants.models.PARKING
            }
        ],
        dayHalfHours: {
            type: Number,
            required: true,
            default: 0
        },
        nightHalfHours: {
            type: Number,
            required: true,
            default: 0
        },
        totalFee: {
            type: Number,
            required: true,
            default: 0
        }
    });

ParkingInvoiceSchema.index({ year: 1, month: 1, customer: 1 }, {unique:true});

module.exports = mongoose.model(constants.models.PARKING_INVOICE, ParkingInvoiceSchema);