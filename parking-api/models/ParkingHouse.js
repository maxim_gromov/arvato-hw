let mongoose   = require('mongoose'),
    Schema     = mongoose.Schema,
    constants  = require('../app.constants'),

    ParkingHouseSchema = new Schema({
        name: {
            type: String,
            required: true
        }
    });

module.exports = mongoose.model(constants.models.PARKING_HOUSE, ParkingHouseSchema);