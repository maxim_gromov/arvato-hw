let mongoose    = require('mongoose'),
    Schema      = mongoose.Schema,
    constants   = require('../app.constants'),

    ParkingSchema = new Schema({
        customer: {
            type: Schema.ObjectId,
            ref: constants.models.CUSTOMER,
            required: true
        },
        year: {
            type: Number,
            required: true
        },
        month: {
            type: Number,
            required: true
        },
        parkingHouse: {
            type: Schema.ObjectId,
            ref: constants.models.PARKING_HOUSE,
            required: true
        },
        invoice: {
            type: Schema.ObjectId,
            ref: constants.models.PARKING_INVOICE,
            required: false
        },
        arrival: {
            type: String, // Timestamp will be stored
            required:true
        },
        departure: {
            type: String, // Timestamp will be stored
            required:true
        }
    });

ParkingSchema.index({ year: 1, month: 1, customer: 1, arrival: 1, departure: 1 }, {unique:true});

module.exports = mongoose.model(constants.models.PARKING, ParkingSchema);