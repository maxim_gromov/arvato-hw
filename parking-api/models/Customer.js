let mongoose    = require('mongoose'),
    Schema      = mongoose.Schema,
    constants   = require('../app.constants'),

    CustomerSchema = new Schema({
        fullName: {
            type: String,
            required: true
        },
        pricingPlan: {
            type: Schema.ObjectId,
            ref: constants.models.CUSTOMER_PLAN,
            required: true
        }
    });

module.exports = mongoose.model(constants.models.CUSTOMER, CustomerSchema);