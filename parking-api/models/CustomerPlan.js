let mongoose    = require('mongoose'),
    Schema      = mongoose.Schema,
    constants   = require('../app.constants'),

    CustomerPlanSchema = new Schema({
        name: {
            type: String,
            required: true
        },
        monthlyFee: {
            type: Number,
            required: true
        },
        dayHalfHourFee: {
            type: Number,
            required: true
        },
        nightHalfHourFee: {
            type: Number,
            required: true
        },
        limit: {
            type: Number,
            required: false
        }
    });

module.exports = mongoose.model(constants.models.CUSTOMER_PLAN, CustomerPlanSchema);