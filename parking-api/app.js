let express             = require('express'),
    path                = require('path'),
    logger              = require('morgan'),
    bodyParser          = require('body-parser'),
    session             = require('express-session'),

    appDatabaseService  = require('./app.db'),

    parking             = require('./api/parking'),
    customer            = require('./api/customer'),

    app                 = express(),
    appDB               = appDatabaseService.connect();

appDB.on('error', console.error.bind(console, 'MongoDB connection error:'));
appDB.on('open', console.error.bind(console, 'MongoDB connection was successfully established!'));

// Uncomment to populate test data
// appDatabaseService.populateTestData();

// TODO configure it for production
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(session({secret: "s3cret"}));

// Basic API
app.use('/api/customer', customer);
app.use('/api/parking', parking);

// Not found
app.use(function (req, res, next) {
    res.status(404).json({message: "API Endpoint not found."});
});

// Error handler
app.use(function (err, req, res, next) {
    if (err && err.message)
        res.status(400).json(err);
    else
        res.status(500).json({message: "Unknown error"});
});

module.exports = app;
