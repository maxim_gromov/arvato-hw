let constants       = require('../app.constants');
let mongoose = require("mongoose");
let Customer = require('../models/Customer');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();

chai.use(chaiHttp);
describe('Customers', () => {
    beforeEach((done) => { //Before each test we empty the database (good to run this in different, CI db)
        Customer.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test the /GET route
     */
    describe('/GET customer', () => {
        it('it should GET all the customers', (done) => {
            chai.request(app)
                .get('/api/customer')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

});