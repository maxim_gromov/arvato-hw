let parkingService  = {},
    moment          = require('moment'),
    Parking         = require('../models/Parking');

// 3rd party could be what ever service, that will use this one to upload data
// In real world, this service should know exact customerId and parkingHouseId
// Or better would be to have some common UID for customer and parking place for all services
// For example, when driver finished his parking, this endpoint is called to store parking information
parkingService.uploadParkingData = (parkingData) => {
    return new Promise((resolve, reject) => {
        if (!parkingData || !parkingData.customer
                         || !parkingData.arrival
                         || !parkingData.departure
                         || !parkingData.parkingHouse)
        return reject({message: "Required data is missing."});

        let parkingDate     = moment.unix(parkingData.arrival),
            newParking      =   new Parking({
                customer:       parkingData.customer,
                year:           parkingDate.year(),
                month:          parkingDate.month() + 1,                       // Using 1 - 12 instead 0 - 11
                parkingHouse:   parkingData.parkingHouse,
                invoice:        null,
                arrival:        parkingData.arrival,
                departure:      parkingData.departure
            });
        return newParking.save().then(resolve, reject);
    });
};

module.exports = parkingService;