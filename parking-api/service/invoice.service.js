let invoiceService = {},
    ParkingInvoice = require('../models/ParkingInvoice'),
    Parking        = require('../models/Parking'),
    moment         = require('moment');

invoiceService.calculateInvoice = (invoice, pricingPlan, parkings) => {
    let limit = pricingPlan.limit;

    invoice.parkings = parkings.map((item) => {
        return item._id
    });

    invoice.totalFee = pricingPlan.monthlyFee;
    invoice.dayHalfHours = 0;
    invoice.nightHalfHours = 0;

    if (parkings && parkings.length > 0)
        parkings.forEach((item) => invoiceService.processInvoiceParking(invoice, item));

    invoice.totalFee += (invoice.dayHalfHours   * pricingPlan.dayHalfHourFee);
    invoice.totalFee += (invoice.nightHalfHours * pricingPlan.nightHalfHourFee);

    // Limitation
    if (limit && invoice.totalFee > limit)
        invoice.totalFee = limit;
};

invoiceService.processInvoiceParking = (invoice, parking) => {
    let arrive      = moment.unix(parking.arrival).utcOffset(0),
        departure   = moment.unix(parking.departure).utcOffset(0);

    while (arrive.isBefore(departure)) {
        if (invoiceService.isDayTime(arrive)) {
            invoice.dayHalfHours++;
        } else {
            invoice.nightHalfHours++;
        }
        arrive.add(30, 'minutes');
    }
};

invoiceService.isDayTime = (time) => {
    let testDateFrom    = time.clone().hour(7).minute(0).second(0),
        testDateUntil   = time.clone().hour(19).minute(0).second(0);
    return time.isBetween(testDateFrom, testDateUntil);
};

module.exports = invoiceService;