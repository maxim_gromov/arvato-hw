let customerService = {},
    invoiceService  = require('./invoice.service'),
    moment          = require('moment'),
    Customer        = require('../models/Customer'),
    Parking         = require('../models/Parking'),
    ParkingInvoice  = require('../models/ParkingInvoice');

customerService.findAllCustomers = () => {
    return Customer
        .find()
        .populate('pricingPlan');
};

// Customer related objects API
customerService.findCustomerParkings    = (customerId, params, includeHouses) => {
    params = params ? params : {};
    params.customer = customerId;

    return Parking
        .find(params)
        .populate(includeHouses ? 'parkingHouse' : '');
};

customerService.findCustomerInvoices    = (customerId, params) => {
    params = params ? params : {};
    params.customer = customerId;

    return ParkingInvoice
        .find(params);
};

customerService.generateCustomerInvoice = (customerId, params) => {
    let invoice       = params,
        collectedData = [
            customerService.findCustomerParkings(customerId, params, false),
            customerService.findCustomerPlan(customerId)
        ];

    invoice.customer  = customerId;
    return new Promise((resolve, reject) => {
        let currentMonth = moment().month() + 1,
            currentYear  = moment().year();

        if (!params || !params.year || !params.month)
            return reject({ message: "No year / month specified for query." });
        if (currentYear == params.year && currentMonth <= params.month)
            return reject({ message: "Invoice cannot be calculated before end of month" });

        return Promise
            .all(collectedData)
            .then((collectedData) => {
                let customerParkings    = collectedData[0],
                    customerPlan        = collectedData[1];
                if (!customerPlan)
                    return reject({ message: "Could not generate invoice - no pricing plan for customer is defined." });
                invoiceService.calculateInvoice(invoice, customerPlan, customerParkings);

                invoice = new ParkingInvoice(invoice);
                return invoice.save().then(resolve, reject);
            });
    });
};

// Find customer plan for calculating invoice
customerService.findCustomerPlan        = (customerId) => {
    return Customer
        .findOne({ _id: customerId }, 'pricingPlan -_id')
        .populate('pricingPlan')
        .then((customer) => {
            return customer ? customer.pricingPlan : null;
        });
};

module.exports = customerService;