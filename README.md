# Implementation

## Database
1. Using Mongoose within remote Mongo DB server from mlab.com
2. Database structure is described in models/_db_design.png

## Services
1. Installed moment.js for better time handling

## API
* _GET_   **/api/customer**	                -	for authentication (just return all customers)
* _GET_   **/api/customer/:id/parking**	    -	get customer parkings
* _GET_   **/api/customer/:id/invoice**     -   get customer invoices
* _POST_  **/api/customer/:id/invoice**     -   generate invoice based on usage

* _POST_  **/api/parking**                  -   for parking data import
( Example body: {
                	"arrival": 1479190022,
                	"departure": 1479199022,
                	"parkingHouse": "58bc2636100ff13150069c4f",
                	"customer": "58bc2638100ff13150069c51" 
                })

Prepared libs for testing, but no tests yet.

## Front - end
* Used libs - bootstrap css and prime ng
* app.component     - just to select customer to login with
* parking-list.component - for showing parkings (table, button), showing invoice details

There are no tests yet.
Front end was done in 2 hours.

## RUN
1. Back end - npm start in parking-api
2. Front end - npm start in parking-ui

I have there already some data in DB to test.
You can see customer / parking place id - s in responses..
Also you can change mongo db connection string in app.constants.
